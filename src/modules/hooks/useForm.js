import { useState } from "react"

export default function useForm(
  initialValues,
  validateOnChange = false,
  validate
) {
  const [values, setValues] = useState(initialValues)
  const [errors, setErrors] = useState({})

  const handleInputChange = event => {
    const { name, value } = event.target
    setValues({
      ...values,
      [name]: value,
    })
    if (validateOnChange) {
      validate({ [name]: value })
    }
  }

  const resetForm = () => {
    setValues(initialValues)
    setErrors({})
  }

  return { values, setValues, resetForm, errors, setErrors, handleInputChange }
}
