import React, {useState} from "react"
import {Link} from 'react-router-dom'
import Avatar from "@material-ui/core/Avatar"
import {CssBaseline, Container, TextField, Grid, Typography, Button} from "@material-ui/core"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import { makeStyles } from "@material-ui/core/styles"
import { useForm } from '../../hooks'
import {useDispatch, useSelector} from 'react-redux'
import { signUp } from "../../store/slices/signupSlice"
import './style/signup.css'
import { ROUTES } from "../../services/common-services/routes"

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

const initialValues = {
  email: '',
  password: '',
  confirmPassword: ''
}

export default function SignUp() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.signUp.loading)
  const [complete, setComplete] = useState(false)
  const [error, setError] = useState(false)

  const validate = (fieldValues = values) => {
    let temp = { ...errors }
    if ("email" in fieldValues)
      temp.email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        fieldValues.email
      )
        ? ""
        : "Invalid Email"

    if ("password" in fieldValues)
      temp.password = fieldValues.password.length > 3 ? "" : "Min 4 Characters"
    if ("confirmPassword" in fieldValues)
      temp.confirmPassword =
        values.password === fieldValues.confirmPassword
          ? ""
          : "Password does not match"

    setErrors({
      ...temp,
    })

    // eslint-disable-next-line eqeqeq
    if (fieldValues == values) return Object.values(temp).every(x => x === "")
  }
  const { values, handleInputChange, resetForm, errors, setErrors } = useForm(
    initialValues,
    true,
    validate
  )

  const onSubmit = e => {
    e.preventDefault()
    const { email, password } = values
    if (validate(values)) {
      setError(false)
      setComplete(false)
      dispatch(signUp(email, password))
        .then(res => setComplete(true))
        .catch(error => setError(true))
      resetForm()
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={onSubmit}>
          {complete && (
            <h2 className="info-msg success">
              Sign up complete you can now login
            </h2>
          )}
          {!complete && error && (
            <h2 className="info-msg success">{error.message}</h2>
          )}
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="none"
                value={values.email}
                onChange={handleInputChange}
                error={!!errors.email}
                helperText={errors.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                value={values.password}
                onChange={handleInputChange}
                error={!!errors.password}
                helperText={errors.password}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirmPassword"
                label="Confirm Password"
                type="password"
                id="confirmPassword"
                value={values.confirmPassword}
                onChange={handleInputChange}
                error={!!errors.confirmPassword}
                helperText={errors.confirmPassword}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={isLoading}
          >
            {isLoading ? "please wait..." : "Sign Up"}
          </Button>
          <Grid container justify="center">
            <Grid item>
              <Link to={ROUTES.LOGIN}>Already have an account? Sign in</Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  )
}