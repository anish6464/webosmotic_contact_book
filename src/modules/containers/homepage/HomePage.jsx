import React, { useEffect, useRef, useState } from "react"
import { Header, AddContact, EditContact } from "../../components"
import { ContactList } from "../../containers"
import {
  Avatar,
  Button,
  Container,
  Divider,
  Fade,
  InputAdornment,
  List,
  ListItem,
  Paper,
  Popper,
  TextField,
  Typography,
} from "@material-ui/core"
import SearchIcon from "@material-ui/icons/Search"
import "./styles/homepage.css"
import { useDispatch, useSelector } from "react-redux"
import {
  addContact,
  updateContact,
  searchContactListStarted,
} from "../../store/slices/contactSlice"
import PersonAddIcon from "@material-ui/icons/PersonAdd"
import { contactListSelector } from "../../store/slices/contactSlice"
import { ROUTES } from "../../services/common-services/routes"
import {
  getFilteredList,
  getHighlightedText,
} from "./services/homepageServices"

export default function HomePage(props) {
  const [editContactId, setEditContactId] = useState(null)
  const [showEditContact, setShowEditContact] = useState(false)
  const [showAddContactForm, setShowAddContactForm] = useState(false)
  const dispatch = useDispatch()
  const currentUser = useSelector(state => state.login.user)
  const [searchText, setSearchText] = useState("")
  const [typingTimeout, setTypingTimeout] = useState(0)
  const inputButtonRef = useRef()
  const allContactList = useSelector(contactListSelector)

  useEffect(() => {
    if (!currentUser?.email) props.history.push(ROUTES.LOGIN)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const onSearchTextChange = e => {
    if (typingTimeout) {
      clearTimeout(typingTimeout)
    }
    setSearchText(e.target.value)

    setTypingTimeout(
      setTimeout(() => dispatch(searchContactListStarted(e.target.value)), 1000)
    )
  }

  const onRowClick = contactId => {
    setEditContactId(contactId)
    setShowEditContact(true)
  }

  const addContactClickHandler = () => {
    setShowAddContactForm(true)
  }

  const onAddContactCancel = () => {
    setShowAddContactForm(false)
  }

  const onAddContactSubmit = values => {
    dispatch(addContact(values))
    setShowAddContactForm(false)
  }

  const onEditCancel = () => {
    setEditContactId(null)
    setShowEditContact(false)
  }

  const onUpdateContact = newData => {
    dispatch(updateContact(editContactId, newData))
    setShowEditContact(false)
    setEditContactId(null)
  }

  const typeAheadSuggestionList = getFilteredList(allContactList, searchText)

  return (
    <div className="homepage">
      <nav className="nav">
        <Header onAddContact={addContactClickHandler} history={props.history} />
      </nav>
      <main className="content-container">
        <Container maxWidth="lg" fixed className="container">
          <div className="header-container">
            <div className="search-container">
              <TextField
                variant="outlined"
                margin="dense"
                value={searchText}
                onChange={onSearchTextChange}
                placeholder="Search by name"
                size="small"
                fullWidth
                ref={inputButtonRef}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  ),
                }}
              />

              <Popper
                open={!!searchText}
                anchorEl={inputButtonRef.current}
                transition
                className="typeahead-popper"
                placement="bottom-start"
              >
                {({ TransitionProps }) => (
                  <Fade {...TransitionProps} timeout={350}>
                    <Paper>
                      <List>
                        {typeAheadSuggestionList.length === 0 ? (
                          <ListItem>
                            <Typography>No match found</Typography>
                          </ListItem>
                        ) : (
                          typeAheadSuggestionList.map(contact => {
                            return (
                              <ListItem key={contact.id}>
                                <Avatar
                                  src={contact.img}
                                  className="typeahead-avatar"
                                />
                                {getHighlightedText(contact.name, searchText)}
                                <Divider />
                              </ListItem>
                            )
                          })
                        )}
                      </List>
                    </Paper>
                  </Fade>
                )}
              </Popper>
            </div>
            <div className="add-button-container">
              <Button
                className="add-contact-btn"
                variant="contained"
                color="secondary"
                onClick={addContactClickHandler}
              >
                <PersonAddIcon fontSize="small" /> Add Contact
              </Button>
            </div>
          </div>
          <ContactList searchParameter onRowClick={onRowClick} />
        </Container>
        {showAddContactForm && (
          <AddContact
            open={showAddContactForm}
            onCancel={onAddContactCancel}
            onSubmit={onAddContactSubmit}
          />
        )}
        {showEditContact && (
          <EditContact
            open={showEditContact}
            contactId={editContactId}
            onCancel={onEditCancel}
            onSubmit={onUpdateContact}
          />
        )}
      </main>
    </div>
  )
}
