import React, {useState} from "react"
import {Link} from 'react-router-dom'
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import {CssBaseline, Container, Grid, TextField, Typography} from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import { useForm } from '../../hooks'
import { useDispatch } from "react-redux"
import { login } from "../../store/slices/loginSlice"
import "./styles/login.css"
import { ROUTES } from "../../services/common-services/routes"

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

const initialValues = {
  email: '',
  password: ''
}

export default function Login(props) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)

  const validate = (fieldValues = values) => {
    let temp = { ...errors }
    if ("email" in fieldValues)
      temp.email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        fieldValues.email
      )
        ? ""
        : "Invalid Email"

    if ("password" in fieldValues)
      temp.password = fieldValues.password.length > 3 ? "" : "Min 4 Characters"
    if ("confirmPassword" in fieldValues)
      temp.confirmPassword =
        values.password === fieldValues.confirmPassword
          ? ""
          : "Password does not match"

    setErrors({
      ...temp,
    })

    // eslint-disable-next-line eqeqeq
    if (fieldValues == values) return Object.values(temp).every(x => x === "")
  }
  const {values, handleInputChange, resetForm, errors, setErrors} = useForm(initialValues, true, validate)

  const onSubmit = (event) => {
    event.preventDefault()
    const { email, password } = values
    setError(false)
    setLoading(true)

    dispatch(login(email, password))
      .then(res => {
        setLoading(false)
        resetForm()
        props.history.push(ROUTES.BASE)
      })
      .catch(error => {
        setError(true)
        resetForm()
        setLoading(false)
      })
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={onSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            value={values.email}
            onChange={handleInputChange}
            error={!!errors.email}
            helperText={errors.email}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={values.password}
            onChange={handleInputChange}
            error={!!errors.password}
            helperText={errors.password}
          />
          <div className="login-error">
            {error && "Invalid username or password"}
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
          >
            {loading ? "loading..." : "Sign In"}
          </Button>
          <Grid container justify="center">
            <Grid item>
              <Link to={ROUTES.SINGUP}>Don't have an account? Sign Up</Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  )
}