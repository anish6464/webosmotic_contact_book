import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Paper from "@material-ui/core/Paper"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import { contactListSelector } from "../../store/slices/contactSlice"
import { useSelector } from "react-redux"
import { Avatar } from "@material-ui/core"
import "./styles/contact-list.css"
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty'

const columns = [
  { id: "name", label: "Name", minWidth: 170 },
  { id: "email", label: "Email", minWidth: 100 },
  { id: "phone", label: "Phone", minWidth: 170, format: value => value },
]

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: "calc(100vh - 150px)",
  },
})

export default function ContactList(props) {
  const classes = useStyles()
  const contactList = useSelector(contactListSelector)
  
  const onRowClickHandler = contactId => {
    console.log("row Click on contactList")
    props.onRowClick(contactId)
  }

  return (
    <Paper className={classes.root}>
      {contactList && contactList.length>0?<TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {contactList.map(contact => (
              <TableRow
                hover
                key={contact.id}
                onClick={() => onRowClickHandler(contact)}
              >
                <TableCell component="div">
                  <div className="with-avatar">
                    {<Avatar alt="Remy Sharp" src={contact.img} />}
                    <span>{contact.name}</span>
                  </div>
                </TableCell>
                <TableCell>{contact.email}</TableCell>
                <TableCell>{contact.phone}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      :<div className="no-contacts-msg"><HourglassEmptyIcon />No contacts</div>}
    </Paper>
  )
}