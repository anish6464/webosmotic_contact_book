import { createSlice } from "@reduxjs/toolkit"
import LoginServices from "../../services/login-services"

const initialState = {
  signUpComplete: false,
  loading: false,
  error: null,
}
export const signUpSlice = createSlice({
  name: "signup",
  initialState,
  reducers: {
    signUpStart: state => {
      state.signUpComplete = false
      state.loading = true
      state.error = null
    },
    signUpSuccess: state => {
      state.error = null
      state.signUpComplete = true
      state.loading = false
    },
    signUpFail: (state, action) => {
      state.error = action.payload
      state.signUpComplete = false
      state.loading = false
    },
  },
})

export const { signUpFail, signUpStart, signUpSuccess } = signUpSlice.actions

export const signUp = (email, password) => async dispatch => {
  dispatch(signUpStart())
  return await LoginServices.signUp(email, password)
      .then(res  => dispatch(signUpSuccess()))
      .catch(err => dispatch(signUpFail(err)))
}
export default signUpSlice.reducer
