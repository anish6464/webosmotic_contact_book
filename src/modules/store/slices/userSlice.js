import { createSlice } from "@reduxjs/toolkit"

const initialState = {
  user: {},
}
export const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    addUser: (state, action) => {
      const { email } = action.payload
      state.user[email] = action.payload
    },
  },
})

export const { addUser } = userSlice.actions

export default userSlice.reducer
