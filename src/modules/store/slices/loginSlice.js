import { createSlice } from "@reduxjs/toolkit"
import LoginServices from "../../services/login-services"

const initialState = {
  success: false,
  loading: false,
  error: null,
  user: null,
}
export const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    loginStart: state => {
      state.success = false
      state.loading = true
    },
    loginSuccess: (state, action) => {
      state.success = true
      state.error = null
      state.loading = false
      state.user = action.payload
    },
    loginFail: (state, action) => {
      state.success = false
      state.loading = false
      state.error = action.payload
    },
  },
})

export const { loginFail, loginStart, loginSuccess } = loginSlice.actions

export const login = (email, password) => async dispatch => {
  dispatch(loginStart())
  return LoginServices.signIn(email, password)
    .then(res => dispatch(loginSuccess(res.user)))
    .catch(error => dispatch(loginFail(error)))
}
export default loginSlice.reducer
