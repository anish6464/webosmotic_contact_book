import { createSlice } from "@reduxjs/toolkit"

function uuidv4() {
  return Math.random().toString()
}

const initialState = {
  contactList: {},
  loader: false,
  searchedContacts: {},
  queryText: ""
}

export const contactSlice = createSlice({
  name: "contacts",
  initialState,
  reducers: {
    addUserContact: (state, action) => {
      const id = uuidv4()
      const newContact = {
        id: id,
        ...action.payload.contactData,
      }
      state.contactList[action.payload.userId] = {
        ...state.contactList[action.payload.userId],
        [id]: newContact,
      }
    },
    updateUserContact: (state, action) => {
      const { userId, contactId, contactData } = action.payload
      state.contactList[userId][contactId.id] = contactData
    },
    filterContactBySearchStarted: (state,action) => {
      state.loader = true
      state.queryText = action.payload
    }
  },
})

const { addUserContact, updateUserContact, filterContactBySearchStarted, filterContactBySearchSuccess} = contactSlice.actions

export const addContact = contactData => (dispatch, getStore) => {
  const currentUserId = getStore().login.user.email
  dispatch(addUserContact({ userId: currentUserId, contactData }))
}

export const updateContact = (contactId, contactData) => (
  dispatch,
  getStore
) => {
  const currentUserId = getStore().login.user.email
  dispatch(updateUserContact({ userId: currentUserId, contactId, contactData }))
}

export const contactListSelector = state => {
  const currentUser = state.login.user
  let allContactListAsArray = []
  const query = state.contact.queryText || ""
  if (currentUser) {
    const contactListOfCurrentUser = state.contact.contactList[currentUser.email]
    if(!contactListOfCurrentUser) return []
      allContactListAsArray = Object.keys(contactListOfCurrentUser).map(
        contactId => {
          return state.contact.contactList[currentUser.email][contactId]
        })

    if (contactListOfCurrentUser && query === "") {
      return allContactListAsArray
    } else
      return allContactListAsArray.filter(
        contact =>
          contact.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
      )
  }
  return []
}

export const searchContactListStarted = (searchValue) => dispatch => {
  dispatch(filterContactBySearchStarted(searchValue))
}

export const searchContactListSuccess = (searchValue) => dispatch => {
  dispatch(filterContactBySearchSuccess(searchValue))
}

export default contactSlice.reducer
