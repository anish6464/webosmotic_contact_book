import { configureStore } from "@reduxjs/toolkit"
import loginSlice from "../store/slices/loginSlice"
import signUpSlice from "../store/slices/signupSlice"
import contactSlice from "../store/slices/contactSlice"
import userSlice from "../store/slices/userSlice"


export default configureStore({
  reducer: {
    login: loginSlice,
    signUp: signUpSlice,
    contact: contactSlice,
    user: userSlice,
  },
})
