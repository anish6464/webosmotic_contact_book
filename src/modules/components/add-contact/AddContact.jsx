import React from "react"
import { Popup, ProfilePicture } from "../../components"

import { Button, TextField } from "@material-ui/core"
import { useForm } from "../../hooks"
import "./styles/add-contact.css"

const initialValues = {
  name: "",
  email: "",
  phone: "",
  img: "",
}

export default function AddContact(props) {
  const { values, handleInputChange } = useForm(initialValues)

  const onSubmit = e => {
    e.preventDefault()
    props.onSubmit(values)
  }

  return (
    <Popup open={props.open} title="Add Contact">
      <form autoComplete="none" onSubmit={onSubmit}>
        <ProfilePicture
          img={values.img}
          onSave={img =>
            handleInputChange({ target: { name: "img", value: img } })
          }
        />

        <TextField
          margin="normal"
          variant="outlined"
          fullWidth
          name="name"
          label="Name"
          autoComplete="none"
          value={values.name}
          onChange={handleInputChange}
        />
        <TextField
          margin="normal"
          variant="outlined"
          fullWidth
          name="email"
          label="Email"
          autoComplete="none"
          value={values.email}
          onChange={handleInputChange}
        />
        <TextField
          margin="normal"
          variant="outlined"
          fullWidth
          name="phone"
          label="Phone"
          type="number"
          autoComplete="none"
          value={values.phone}
          onChange={handleInputChange}
        />
        <div className="action-button-container">
          <Button className="btn" variant="contained" onClick={props.onCancel}>
            Cancel
          </Button>
          <Button
            type="submit"
            className="btn"
            variant="contained"
            color="primary"
          >
            Submit
          </Button>
        </div>
      </form>
    </Popup>
  )
}
