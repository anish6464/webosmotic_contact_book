import React from "react"
import { Popup, ProfilePicture } from "../../components"
import { Button, TextField } from "@material-ui/core"
import { useForm } from "../../hooks"

export default function EditContact(props) {
  const { values, handleInputChange } = useForm(props.contactId)
  const onSave = () => {
    props.onSubmit(values)
  }

  return (
    <Popup open={props.open} onClose={props.onCancel} title="Edit Contact">
      <form autoComplete="none">
        <ProfilePicture
          img={values.img}
          onSave={img =>
            handleInputChange({ target: { name: "img", value: img } })
          }
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          name="name"
          label="Name"
          autoComplete="none"
          value={values.name}
          onChange={handleInputChange}
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          name="email"
          label="Email"
          autoComplete="none"
          value={values.email}
          onChange={handleInputChange}
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          name="phone"
          label="Phone"
          type="number"
          autoComplete="none"
          value={values.phone}
          onChange={handleInputChange}
        />
        <div className="action-button-container">
          <Button className="btn" variant="contained" onClick={props.onCancel}>
            Cancel
          </Button>
          <Button
            type="submit"
            color="primary"
            className="btn"
            variant="contained"
            onClick={onSave}
          >
            Submit
          </Button>
        </div>
      </form>
    </Popup>
  )
}
