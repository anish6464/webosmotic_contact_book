import React, { PureComponent } from "react"
import ReactCrop from "react-image-crop"
import "react-image-crop/dist/ReactCrop.css"
import "./styles/profile-pic-editor.css"
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@material-ui/core"

export default class ProfilePicEditor extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      src: this.props.imgSrc,
      crop: {
        unit: "%",
        width: 70,
        x: 15,
        y: 10,
        aspect: 1,
      },
      croppedImageUrl: "",
    }
  }

  imageRef
  fileUrl

  onImageLoaded = image => {
    this.imageRef = image
  }

  onCropComplete = (crop, percentCrop) => {
    this.makeClientCrop(crop)
  }

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop })
  }

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        "newFile.jpeg"
      )
      this.setState({ croppedImageUrl })
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas")
    const scaleX = image.naturalWidth / image.width
    const scaleY = image.naturalHeight / image.height
    if (crop.width) canvas.width = crop.width
    if (crop.height) canvas.height = crop.height

    const ctx = canvas.getContext("2d")
    if (
      ctx &&
      crop.x !== undefined &&
      crop.y !== undefined &&
      crop.width !== undefined &&
      crop.height !== undefined
    ) {
      ctx.drawImage(
        image,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        crop.width,
        crop.height
      )
    }

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error("Canvas is empty"))
          return
        }
        const reader = new FileReader()
        reader.readAsDataURL(blob)
        reader.onloadend = function () {
          const base64data = reader.result
          if (base64data) resolve(base64data)
        }
      }, "image/jpeg")
    })
  }

  getLoader = () => (
    <div className="loader-wrapper">
      <CircularProgress />
    </div>
  )

  getEditor = () => {
    const { src, crop, croppedImageUrl } = this.state
    const { isLoading } = this.props
    return (
      <div className="image-editor-wrapper">
        <div className="image-editor-container">
          <div className="img-resize-container">
            {src && (
              <ReactCrop
                className="react-crop"
                src={src}
                crop={crop}
                keepSelection
                circularCrop
                renderSelectionAddon={() => {
                  return isLoading ? this.getLoader() : null
                }}
                ruleOfThirds
                minWidth={50}
                minHeight={50}
                imageStyle={{
                  width: "auto",
                  height: "200px",
                  objectFit: "cover",
                }}
                onImageLoaded={this.onImageLoaded}
                onComplete={this.onCropComplete}
                onChange={this.onCropChange}
              />
            )}
          </div>
          {croppedImageUrl && (
            <div className="preview-img-container">
              <Typography
                className="preview-text"
                variant="caption"
                component="div"
                align="center"
                color="textPrimary"
              >
                Preview
              </Typography>
              <img alt="Crop" className="preview-image" src={croppedImageUrl} />
              {isLoading && (
                <div className="loader-container">{this.getLoader()}</div>
              )}
            </div>
          )}
        </div>
      </div>
    )
  }

  render() {
    const { croppedImageUrl } = this.state
    const { open, onCancel, onSave, isLoading } = this.props
    return (
      <Dialog
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="sm"
        fullWidth
      >
        <DialogTitle id="customized-dialog-title">
          User Profile Photo
        </DialogTitle>
        <DialogContent>
          <div style={{ width: "100%" }}>{this.getEditor()}</div>
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="primary" disabled={isLoading}>
            Cancel
          </Button>
          <Button
            disabled={isLoading}
            variant="contained"
            onClick={() => onSave(croppedImageUrl)}
            disableElevation
            color="primary"
          >
            {isLoading && <CircularProgress size={15} />}Save
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
