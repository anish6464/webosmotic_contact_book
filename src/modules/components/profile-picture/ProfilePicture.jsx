import { Avatar } from "@material-ui/core"
import Skeleton from "@material-ui/lab/Skeleton"
import { PhotoCamera } from "@material-ui/icons"
import React, { useEffect, useRef, useState } from "react"
import { ProfilePicEditor } from "../../components"
import "./styles/profile-picture.css"

export default function ProfilePic(props) {
  const fileInputRef = useRef()
  const [showProfileEditor, setShowProfileEditor] = useState(false)

  const [editorImgSrc, setEditorImgSrc] = useState(props.img)

  const [isProfilePictureUploading, setIsProfilePictureUploading] = useState(
    false
  )
  const [img, setImg] = useState(props.img || "")

  useEffect(() => {
    if (fileInputRef.current.value !== "") avatarClickHandler()
  }, [editorImgSrc])

  const avatarClickHandler = () => {
    setShowProfileEditor(true)
  }

  const handleFileChange = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader()
      reader.addEventListener("load", () => setEditorImgSrc(reader.result))
      reader.readAsDataURL(e.target.files[0])
    }
  }

  const resetFileInput = () => {
    if (fileInputRef.current) fileInputRef.current.value = ""
  }

  const profilePicSaveHandler = croppedImageUrl => {
    console.log("croppedImageUrl", croppedImageUrl)
    setIsProfilePictureUploading(true)
    setImg(croppedImageUrl)
    setShowProfileEditor(false)
    setIsProfilePictureUploading(false)
    resetFileInput()
    props.onSave(croppedImageUrl)
  }

  return (
    <>
      <div className="profile-head">
        <div className="profile-picture-container">
          {false ? (
            <Skeleton
              animation="wave"
              variant="circle"
              height={200}
              width={200}
            />
          ) : (
            <div className="picture-wrapper">
              <Avatar
                alt="avatar"
                src={img}
                variant="circular"
                className="profile-avatar"
              />
              <input
                ref={fileInputRef}
                accept="image/*"
                style={{ display: "none" }}
                id="raised-button-file"
                type="file"
                onChange={handleFileChange}
              />
              <label htmlFor="raised-button-file" className="edit">
                <PhotoCamera
                  classes={{ root: "edit-icon" }}
                  color="action"
                  fontSize="large"
                />
              </label>
            </div>
          )}
        </div>
      </div>

      <div className="profile-editor-popup-container">
        {showProfileEditor && (
          <ProfilePicEditor
            imgSrc={editorImgSrc}
            open={showProfileEditor}
            onCancel={() => {
              setShowProfileEditor(false)
              resetFileInput()
            }}
            onSave={profilePicSaveHandler}
            isLoading={isProfilePictureUploading}
          />
        )}
      </div>
    </>
  )
}
