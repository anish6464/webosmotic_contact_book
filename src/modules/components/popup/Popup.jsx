import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import DialogTitle from "@material-ui/core/DialogTitle"

const useStyles = makeStyles(theme => ({
  dialog: {
    position: "absolute",
    top: "10px",
  },
}))

export default function Popup(props) {
  const classes = useStyles()

  return (
    <Dialog
      classes={{
        paper: classes.dialog,
      }}
      fullWidth
      maxWidth={"sm"}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="max-width-dialog-title"
    >
      <DialogTitle id="max-width-dialog-title">{props.title}</DialogTitle>
      <DialogContent dividers>{props.children}</DialogContent>
    </Dialog>
  )
}
