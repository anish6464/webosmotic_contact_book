const users = {}
const DELAY = 1000

export default class LoginServices {
  static signUp(email, password) {
    return new Promise((res, rej) => {
      setTimeout(() => {
        if (email in users) rej({ message: "Email is already exist" })
        users[email] = {
          email,
          password,
        }
        res()
      }, DELAY)
    })
  }

  static signIn(email, password) {
    return new Promise((res, rej) => {
      let isValid = false
      setTimeout(() => {
        if (users[email]) {
          if (users[email].password === password) {
            isValid = true
          }
        }
        if (isValid) res({ user: { ...users[email] } })
        else rej({ message: "Invalid Email/Password" })
      }, DELAY)
    })
  }
}
