import React from "react"
import "./App.css"
import { BrowserRouter as Router } from "react-router-dom"
import { Switch, Route } from "react-router-dom"
import { Login, Signup, HomePage } from "./modules/containers"
import { ROUTES } from "./modules/services/common-services/routes"

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path={ROUTES.LOGIN} component={Login} />
          <Route exact path={ROUTES.SINGUP} component={Signup} />
          <Route exact path={ROUTES.BASE} component={HomePage} />
        </Switch>
      </Router>
    </>
  )
}

export default App
